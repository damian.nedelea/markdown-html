export const markdownFilter = (req, file, callback) => {
  console.log(file);
  if (
    !file.mimetype.match(
      /(text\/markdown|text\/x-markdown|application\/octet-stream)/,
    )
  ) {
    return callback(new Error('Only markdown files are allowed!'), false);
  }
  callback(null, true);
};
