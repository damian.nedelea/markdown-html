import { Controller, Get, Res } from '@nestjs/common';
import { Response } from 'express';
import path from 'path';

@Controller()
export class AppController {
  @Get()
  public getIndexHtml(@Res() res: Response): void {
    res.sendFile(path.join(process.cwd(), 'public', 'index.html'));
  }
}
