import path from 'path';

export const HTML_POSTS_PATH = path.join(
  process.cwd(),
  'public',
  'posts',
  'html',
);
export const MARKDOWN_POSTS_PATH = path.join(
  process.cwd(),
  'public',
  'posts',
  'markdown',
);
