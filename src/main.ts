import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { markdownEngine } from './view-engines';
import path from 'path';
import {existsSync, mkdirSync} from 'fs';
import {HTML_POSTS_PATH, MARKDOWN_POSTS_PATH} from './constants';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(path.join(__dirname, '..', 'public'));
  app.engine('md', markdownEngine);
  app.set('views', './public/posts');
  app.set('view engine', 'md');

  if (!existsSync(HTML_POSTS_PATH)) {
    mkdirSync(HTML_POSTS_PATH, { recursive: true });
  }
  if (!existsSync(MARKDOWN_POSTS_PATH)) {
    mkdirSync(MARKDOWN_POSTS_PATH, { recursive: true });
  }
  await app.listen(process.env.PORT);
}
void bootstrap();
