export enum PostFileTypeEnum {
  MARKDOWN = 'markdown',
  HTML = 'html',
}
