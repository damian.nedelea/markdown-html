import { MulterModule } from '@nestjs/platform-express';
import { Module } from '@nestjs/common';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { PostsMulterConfig } from './config/posts-multer.config';

@Module({
  imports: [
    MulterModule.registerAsync({
      useClass: PostsMulterConfig,
    }),
  ],
  controllers: [PostsController],
  providers: [PostsService],
})
export class PostsModule {}
