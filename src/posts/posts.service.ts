import { Injectable, NotFoundException } from '@nestjs/common';
import { Response } from 'express';
import { readdir } from 'fs/promises';
import slugify from 'slugify';
import { PostFileTypeEnum } from './enums/PostFileTypeEnum';
import { HTML_POSTS_PATH, MARKDOWN_POSTS_PATH } from '../constants';

type FileData = { type: PostFileTypeEnum; fileName: string; filePath: string };

@Injectable()
export class PostsService {
  public async getPost(
    lookedFile: string,
    disableCache: boolean,
    res: Response,
  ): Promise<void> {
    const { fileName, type, filePath } = await this.findPostFile(
      lookedFile,
      disableCache,
    );

    if (!fileName) {
      throw new NotFoundException('Post not found!');
    }

    if (type === PostFileTypeEnum.HTML) {
      return res.sendFile(filePath);
    }

    return res.render(`${type}/${fileName}`);
  }

  private async findPostFile(
    fileName: string,
    disableCache,
  ): Promise<FileData> {
    const slugifiedFileName = slugify(fileName, { lower: true }).replace(
      /(.md|.html)/g,
      '',
    );

    const markDownFileNames = await readdir(MARKDOWN_POSTS_PATH);
    const htmlFileNames = await readdir(HTML_POSTS_PATH);

    let htmlFileName;

    if (!disableCache) {
      htmlFileName = htmlFileNames.find(
        (file) => file.replace('.html', '') === slugifiedFileName,
      );
    }

    const markDownFileName = markDownFileNames.find(
      (file) =>
        slugify(file, { lower: true }).replace('.md', '') === slugifiedFileName,
    );

    return {
      type: htmlFileName ? PostFileTypeEnum.HTML : PostFileTypeEnum.MARKDOWN,
      fileName: htmlFileName || markDownFileName,
      filePath: htmlFileName
        ? `${HTML_POSTS_PATH}/${htmlFileName}`
        : `${MARKDOWN_POSTS_PATH}/${markDownFileName}`,
    };
  }
}
