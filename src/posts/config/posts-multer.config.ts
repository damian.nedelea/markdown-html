import { Injectable } from '@nestjs/common';
import {
  MulterModuleOptions,
  MulterOptionsFactory,
} from '@nestjs/platform-express';
import { existsSync, mkdirSync } from 'fs';
import { diskStorage } from 'multer';
import { MARKDOWN_POSTS_PATH } from '../../constants';
import { markdownFilter } from '../../utils/file-upload.utils';

@Injectable()
export class PostsMulterConfig implements MulterOptionsFactory {
  public async createMulterOptions(): Promise<MulterModuleOptions> {
    return {
      limits: {
        fileSize: 20 * 1024 * 1024,
        fieldSize: 20 * 1024 * 1024,
      },
      storage: diskStorage({
        destination: (req, file, callback) => {
          const newDestination = MARKDOWN_POSTS_PATH;
          if (!existsSync(newDestination)) {
            mkdirSync(newDestination, { recursive: true });
          }
          callback(null, newDestination);
        },
        filename: (req, file, callback) => {
          callback(null, file.originalname);
        },
      }),
      fileFilter: markdownFilter,
    };
  }
}
