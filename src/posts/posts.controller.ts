import {
  Controller,
  Get,
  Param,
  Post,
  Query,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { Response } from 'express';
import { PostsService } from './posts.service';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get(':fileName')
  public async getPost(
    @Param('fileName') fileName: string,
    @Query('disableCache') disableCache: string,
    @Res() res: Response,
  ): Promise<void> {
    return this.postsService.getPost(fileName, disableCache === 'true', res);
  }

  @Post('upload-post')
  @UseInterceptors(FileInterceptor('file'))
  public async uploadPost(
    @UploadedFile() file: Express.Multer.File,
  ): Promise<void> {
    console.log(file);
  }
}
