import { readFile, writeFile } from 'fs';
import { Converter, ConverterOptions } from 'showdown';
import slugify from 'slugify';
import path from 'path';
import { HTML_POSTS_PATH } from '../constants';

const engineOptions: ConverterOptions = {
  headerLevelStart: 0,
};

const markdownPathToHtmlName = (filePath: string) => {
  const formattedPath = filePath.replace('/', '\\');
  const fileName = filePath.substring(formattedPath.lastIndexOf('\\') + 1);
  return slugify(fileName, { lower: true }).replace('.md', '.html');
};

export const markdownEngine = (
  filePath: string,
  options: unknown,
  callBack: any,
) => {
  readFile(filePath, (err, content) => {
    if (err) return callBack(err);
    const converter = new Converter(engineOptions);
    const rendered = converter.makeHtml(content.toString());
    writeFile(
      path.join(HTML_POSTS_PATH, markdownPathToHtmlName(filePath)),
      rendered,
      (err) => {
        if (err) return callBack(err);
      },
    );

    return callBack(null, rendered);
  });
};
